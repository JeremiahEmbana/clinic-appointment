import API from '../../base/'

export default {
  namespaced: true,
  state: {
    patients: {},
  },
  getters: {
    GET_PATIENTS(state) {
      return state.patients;
    }
  },
  mutations: {
    SET_PATIENTS(state, data) {
      state.patients = data
    },
    REMOVE_PATIENT(state, id){
     state.patients.data = state.patients.data.filter(patient => {
      return patient.id !== id;
     });
    }
  },
  actions: {
    async getPatients({commit}, page){
      const res = await API.get(`/admin/patients?page=${page}`).then(res => {
        commit('SET_PATIENTS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async searchPatient({commit}, {data, page}){
      const res = await API.post(`/admin/search/patients?page=${page}`, data).then(res => {
        commit('SET_PATIENTS', res.data)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async deletePatient({commit}, id){
      const res = await API.delete(`/admin/patients/${id}`).then(res => {
        commit('REMOVE_PATIENT', id)

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
  },
}
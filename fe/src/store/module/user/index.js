import API from '../../base/'

export default {
  namespaced: true,
  state: {
   appointments: [],
   summary: [],
   billing: {},
  },
  getters: {
    GET_APPOINTMENTS(state) {
      return state.appointments;
    }
  },
  mutations: {
    SET_APPOINTMENTS(state, data) {
      state.appointments = data
    },
    SET_SUMMARY(state, data) {
      state.summary = data
    },
    SET_BILLING(state, data) {
     state.billing = data
   },
  },
  actions: {
    async getAppointments({commit}){
     const res = await API.get('/user/appointment').then(res => {
      commit('SET_APPOINTMENTS', res.data)
     }).catch(err => {
      return err.response;
     })

     return res;
    },
    async getSummary({commit}){
      const res = await API.get('/user/summary').then(res => {
        commit('SET_SUMMARY', res.data)
       }).catch(err => {
        return err.response;
       })
  
       return res;
    },
    async saveAppointment({commit}, data){
      const res = await API.post('/user/appointment', data).then(res => {

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async cancelAppointment({commit}, id){
      const res = await API.put(`/user/appointment/${id}`).then(res => {

        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
  }
}
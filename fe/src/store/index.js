import Vue from 'vue'
import Vuex from 'vuex'

import auth from './module/auth'
import patient from './module/patient'
import user from './module/user'
import appointment from './module/appointment'

Vue.use(Vuex);

export default new Vuex.Store({
 modules: {
  auth, patient, appointment, user
 }
})


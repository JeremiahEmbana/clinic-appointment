<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        return response()->json(Appointment::with(['user', 'user.info'])->get());
    }

    public function getAllAppointments()
    {
        return response()->json(Appointment::with(['user', 'user.info'])->paginate(10));
    }

    public function store(Request $request){
        $data = [
            'user_id' => $request->user_id,
            'type' => $request->name,
            'content' => $request->description,
            'start' => $request->time[0],
            'end' => $request->time[1],
            'status' => 'Approved'
        ];

        $appointment = Appointment::create($data);
        $added_appointment = Appointment::with(['user', 'user.info'])->where('id', $appointment->id)->first();
        return response()->json(['msg' => 'Appointment added successfully!', 'appointment' => $added_appointment], 200);
    }

    public function destroy(Request $request, $id){
        Appointment::destroy($id);
        return response()->json(['msg' => 'Appointment deleted successfully'], 200);
    }

    public function approveAppointment(Request $request, $id){
        $appointment = Appointment::find($id);
        if($appointment){
            $appointment->update(['status' => 'Approved']);
            return response()->json(['msg' => 'Appointment approved successfully'], 200);
        }
        else {
            return response()->json(['msg' => 'Appointment not found'], 200);
        }
    }

    public function searchAppointment(Request $request){
        $appointment = Appointment::whereHas('user.info', function($query){
            $query->where('first_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('middle_name', 'like', '%'.request()->get('search').'%')
            ->orWhere('last_name', 'like', '%'.request()->get('search').'%');
        })->orWhereHas('user', function($query){
            $query->where('id', 'like', '%'.request()->get('search').'%');
        })->orWhere('type', 'like', '%'.request()->get('search').'%')
        ->with(['user', 'user.info'])->paginate(10);

        return response()->json($appointment, 200);
    }
}

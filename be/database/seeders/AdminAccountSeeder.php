<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\AdminInfo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admininfo = AdminInfo::create([
            'first_name' => 'Jeremiah',
            'middle_name' => 'Orpeza',
            'last_name' => 'Embana',
            'gender' => 'Male',
            'contact_number' => '09123456789'
        ]);

        Admin::create([
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin123'),
            'admin_info_id' => $admininfo->id
        ]);
    }
}
